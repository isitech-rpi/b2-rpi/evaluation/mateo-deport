//
//  Personne.hpp
//  Eval
//
//  Created by Matéo Deport on 04/07/2024.
//

#ifndef Personne_hpp
#define Personne_hpp

#include <stdio.h>
#include <string>

using namespace std;

class Personne
{
private:
    string nom;
    
public:
    Personne(string nom);
    
    ~Personne();
    
    string GetNom();
    
    void SetNom(string nom);
};
#endif /* Personne_hpp */
