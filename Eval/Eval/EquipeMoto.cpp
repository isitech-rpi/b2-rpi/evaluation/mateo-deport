//  EquipeMoto.cpp
//  Eval
//
//  Créé par Matéo Deport le 04/07/2024.
//

#include "EquipeMoto.hpp"
#include <string>
#include <iostream>

EquipeMoto::EquipeMoto(string nomEquipe, string nomManager) {
    this->nom = nomEquipe;
    this->manager = new Personne(nomManager);
    
    for (int i = 0; i < 3; ++i) {
            lesPilotes[i] = nullptr;
        }
}

EquipeMoto::~EquipeMoto() {
    delete manager;
}

string EquipeMoto::GetNom() {
    return this->nom;
}

void EquipeMoto::SetNom(string nom) {
    this->nom = nom;
}

Personne* EquipeMoto::GetManager() {
    return this->manager;
}

void EquipeMoto::AddPilote(unsigned int rang, Personne* pilote) {
    if (rang < 3) {
        if (lesPilotes[rang] != nullptr) {
            delete lesPilotes[rang];
        }
        lesPilotes[rang] = pilote;
    } else {
        cout << "Ajout annulé." << endl;
    }
}

Personne** EquipeMoto::GetPilotes() {
    return lesPilotes;
}


