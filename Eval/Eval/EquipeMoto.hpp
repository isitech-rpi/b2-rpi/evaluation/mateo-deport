//
//  EquipeMoto.hpp
//  Eval
//
//  Created by Matéo Deport on 04/07/2024.
//

#ifndef EquipeMoto_hpp
#define EquipeMoto_hpp

#include <stdio.h>
#include <string>
#include "Personne.hpp"

using namespace std;

class EquipeMoto
{
private:
    string nom;
    Personne* manager;
    Personne* lesPilotes[3];
public:
    unsigned int maxPilote = 3;
    
    EquipeMoto(string nomEquipe, string nomManager);
    ~EquipeMoto();
    string GetNom();
    void SetNom(string nom);
    Personne* GetManager();
    void AddPilote(unsigned int rang, Personne* pilote);
    Personne** GetPilotes();
};
#endif /* EquipeMoto_hpp */
