//
//  main.cpp
//  Eval
//
//  Created by Matéo Deport on 04/07/2024.
//

#include <iostream>
#include "Personne.hpp"
#include "EquipeMoto.hpp"

using namespace std;

void AffichePersonne(Personne& xxxx) {
    cout << "Personne(" << &xxxx << "): " << xxxx.GetNom() << endl;
}

Personne* CreerPersonne() {
    string nom;
    cout << "Entrez le nom de la personne : ";
    cin >> nom;
    Personne* personne;
    personne = new Personne(nom);
    return personne;
}

void AfficheEquipeMoto(EquipeMoto* equipe) {
    cout << "Equipe " << equipe->GetNom() << " managé par " << equipe->GetManager()->GetNom() << endl;
    
    // Parcourir les pilotes et les afficher
    Personne** pilotes = equipe->GetPilotes();
    for (int i = 0; i < 3; ++i) {
        if (pilotes[i] != nullptr) {
            cout << "    ->Pilote[" << i << "] = " << pilotes[i]->GetNom() << endl;
        } else {
            cout << "    ->Pas de pilote[" << i << "]" << endl;
        }
    }
}

int main(int argc, const char * argv[]) {
    Personne pilote_1("Fabio");
    
    AffichePersonne(pilote_1);
    
    Personne* pilote_2 = CreerPersonne();
    
    AffichePersonne(*pilote_2);
    
    EquipeMoto* equipe_1;
        
    equipe_1 = new EquipeMoto("YMF", "Jarvis");
        
    cout << "Adresse de equipe_1 : " << equipe_1 << endl;
    cout << "Nom de l'équipe : " << equipe_1->GetNom() << endl;
    cout << "Nom du manager : " << equipe_1->GetManager()->GetNom() << endl;
    
    equipe_1->AddPilote(0, new Personne("Fabio"));
    equipe_1->AddPilote(1, new Personne("Alex"));
        

    AfficheEquipeMoto(equipe_1);
        
        
    delete equipe_1->GetPilotes()[0];
    delete equipe_1->GetPilotes()[1];
    delete equipe_1;
    delete pilote_2;
}
