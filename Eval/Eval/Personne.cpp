//
//  Personne.cpp
//  Eval
//
//  Created by Matéo Deport on 04/07/2024.
//

#include "Personne.hpp"
#include <string>
#include <iostream>

using namespace std;


Personne::Personne(string nom)
{
    this->nom = nom;
}

Personne::~Personne()
{
    cout << "Destruction de l'objet Personne nommé " << nom << endl;
}

string Personne::GetNom()
{
    return this->nom;
}

void Personne::SetNom(string nom)
{
    this->nom = nom;
}

